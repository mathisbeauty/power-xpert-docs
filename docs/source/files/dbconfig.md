# DBConfig

## Introduction

DBConfig is an Angular service which handles all the logic related to requesting, parsing and modifying the DBStructure object, which is a [generic toolkit](./mercury.html#generic-toolkits) that contains the main configuration and state of the backend/gateway.

## DBStructure

DBStructure is the JSON object that contains the following:
- Preferences, or main configuration of the app (`pref`).
- PLC (`plc`). Holds the configuration for the Transfer page.
- Lineups (`lineups`)
- Sections (`sections`)
- Composites (`composites`)
- Elements (`elements`)
- Wires (`wires`)
- Compartments (`compartments`)
- Devices (`devices`)
- Channels (`channels`)

DBStructure is retrieved from the backend and parsed by the DBConfig service.

The resulting object, is stored in the `dbConfig` property, which is where the configuration is accessible to the rest of the app.

### MapObjects - How the items are stored and accessed

All the properties, except `pref` and `plc` are MapObjects, which means that the items from the DBStructure array are stored inside a custom object.

For example, if you want to access the Wire with id 3, you'll do `this.DBConfig.dbConfig.wires[3]`.
If you want to get the items as a plain array, you can access them by using the `structure` property.
E.g. `this.DBConfig.wires.structure` will give access to all the wires as an array.
