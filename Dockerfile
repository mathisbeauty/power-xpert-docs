FROM python:3

WORKDIR /home/app

COPY . .

RUN pip install -r docs/requirements.txt

RUN sphinx-build ./docs/source public

CMD cd public && python3 -m http.server 8000
