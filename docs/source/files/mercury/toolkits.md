
### Toolkit Configurations

A toolkit is a JSON object that contains a part of the configuration of the app.

Each toolkit has a tag associated with it. For example, the toolkit that contains the language settings, has the tag `Locale`.

They can be requested and modified via the `setData` and `getData` methods in the Mercury service.

### Generic Toolkits

Some toolkits are stored as *generic toolkits*.

This means that instead of storing all the configuration as a JSON object the configuration is stored as a string.

The structure of these toolkits is as follows:

```
{
    generic: {
        StringData: <stringified_object>
    }
}
```

`<stringified_object>` is the configuration object converted into a string.

This is the case of, for example, [DBConfig](./dbconfig.html).


They can be requested and modified via the `setGeneric` and `getGeneric` methods in the Mercury service.
