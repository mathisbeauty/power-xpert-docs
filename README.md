# Power Xpert Docs

## Requirements
- Python 3

or

- Docker

## Installation

If you are not using Docker, you'll need to install the requirements by running:
```
pip3 install -r docs/requirements.txt
```

## Build

### Build the docs (Python)

Run:
```
sphinx-build ./docs/source public
```

### Build the docs (Docker)

To build the Docker image, run:
```
docker run -P -i -t power-xpert-docs
```

## Serve

The app is served with a development server at `http://localhost:8000/`.

### Serve the docs (Python)

To quickly see the generated docs, run:
```
cd public
python3 -m http.server 8000
```

### Serve the docs (Docker)

```
docker run -p 8000:8000 -i -t doc
```

## Deployment

The app is configured to be deployed to [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/#getting-started) and/or the [readthedocs](https://readthedocs.com/) hosting.

Follow the instructions for the one that you prefer.
