# Mercury API

## Introduction

The application needs to send and receive messages, in realtime, from a variety of devices of the electrical network.

This is not easily doable with a standard REST API. For this reason, the application communicates with the backend using a [WebSocket](https://en.wikipedia.org/wiki/WebSocket) connection.

On top of that, Eaton has designed and implemented the Mercury protocol.

Mercury is an abstract standard, that means that using WebSocket is one option among many to implement it, see the [Developer Guide](#mercury-15-apis-developer-guide) for more information.

## How it Works

In the Power Xpert Dashboard, the Mercury standard is implemented as an Angular service.

This service allows the application to:
- Connect via WebSocket to a given backend.
- Login
- Request and receive configuration, in the form of [Toolkits](#toolkits)
- Subscribe to realtime updates, usually used for channels and devices' status

The backend, also known as gateway, acts as the intermediary between the devices in the electrical network and the application.

## Toolkits

```eval_rst
.. mdinclude:: mercury/toolkits.md
```

## Mercury 1.5 APIs Developer Guide

```eval_rst
.. mdinclude:: mercury/guide.md
```
