### Link

Here's the official guide from Eaton to develop the Mercury API:

[Guide (.docx format)](../_static/Mercury.APIs.Developer.Guide.1.5.docx)

### What is this guide?

It describes how the Mercury API works and how to develop for it.

### What information contains?

The guide is fairly low level.
Most of the information in it is only useful if you are going to develop a low level API for embedded devices.

Nonetheless, you may find useful insights by skimming over the index and theoretical chapters.
Also, it may help you understand how Mercury works and what devices it's talking to in the app.
