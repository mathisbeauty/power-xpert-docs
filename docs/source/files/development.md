# Development

## Integration Guide

The process that I follow to integrate the UI of the app can be divided into 3 phases:
1. Setup
2. Main refactor
3. Debugging

Here are the steps for each one.

### Setup
1. Open the legacy and the new app, side by side
2. Open the DevTools for both of them
3. Go to the page that you want to integrate and open the code for both apps
4. To start from scratch, I usually create a new file in the new app and copy paste the file from the legacy file as a comment. Then, you can create a new Angular class and start cutting and pasting the old functions one by one

### Main refactor

1. Update old functions (`function() {}`) to arrow functions (`() => {}`)
2. Remove legacy lodash functions with new ones that do the same job
3. Remove the use of the `arguments` keyword
4. Replace legacy services with refactored ones
5. Fix linting issues

### Debugging

After the main refactor is done, the new code passes linting and works somewhat, the most probable thing will be that there will be weird or unexpected errors.

This is because the legacy and new apps aren't exactly similar, and the new code needs to be updated and revised more thoroughly.

What I'll do here is:
1. Log in the console an object from the new code and its corresponding object from the legacy code. Most probably, they will be different.
2. Now, continue logging the different parts of the codebase and going through the legacy code, trying to understand what part is broken in the new app.
3. I recommend appending an identifier to the log messages, specially in the legacy app, otherwise it will be difficult to find the log amidst all the warnings, errors and unrelated messages.
4. After repeating this process for the different methods or steps of the code, you should find the source of the issue, and you can refactor this part of the code to produce the same output/log as the legacy app.
5. Once both apps have the same outputs, that means that the integration is complete.

## Additional Notes

### Settings

All references to files are relative to `src/app/services/mercury` in the new app.

**Note:** This notes apply to the more simple Settings' pages, not all pages will follow this. E.g. `Devices > Setpoint Files`.

In the new app, the data of the Settings pages passes through various stages:

1. Method to load the Settings for a page is called (in Settings service).
2. Define in a constant all the tags of the [Toolkits](./mercury.html#toolkits) related to that page. See `./constants.ts`.
3. Request each toolkit and join all responses in an array.
4. Map the responses to the UI data. See `./mappings.ts`
5. Update the forms in the Settings service.

